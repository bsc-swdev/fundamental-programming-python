import sys
import pickle

# region Global constant

TRANSLATION = {
    "2": "abc",
    "3": "def",
    "4": "ghi",
    "5": "jkl",
    "6": "mno",
    "7": "pqrs",
    "8": "tuv",
    "9": "wxyz"
}

# endregion


def get_file_path(index: int) -> str:
    """
    Gets the file paths for the given index from sys.argv\n
    @return the filename if present, otherwise None
    """
    try:
        return sys.argv[index]
    except IndexError:
        return None


def read_orders(filepath: str) -> set:
    """Deserialises the contents of a filepath, auto closes the file object.\n
    @return a Set object or None if IOError is raised
    """
    try:
        with open(filepath, 'rb') as codes_file:
            # Deserialise and return the Set object
            return pickle.load(codes_file)
    except IOError:
        # Return None if an IOError is thrown when opening file
        return None


def read_words(filepath: str) -> list:
    """Reads each line of word from a file and appends to a List,\n
    auto closes the file object.\n
    @return A List of strings, otherwise None if IOError is raised
    """
    words_list = list()
    try:
        with open(filepath, 'r') as words_file:
            for word in words_file:
                words_list.append(word.strip("\n"))
        return words_list
    except IOError:
        return None


def find_all_possible_combinations(order_num: str) -> list:
    """Returns a List of all possible combinations for a set of numbers,\n
     derivable, by concatenating possible chars of each digit in a tree like\n
     fashion, as per TRANSLATION constant."""
    combo_list = []
    for digit in order_num:
        combo_list = add_digit(digit, combo_list)
    return combo_list


def add_digit(digit: str, combinations: list) -> list:
    """Helper algorithm for FindAllPossibleCombination, used for building\n
    a tree-like pattern as new digits are iterated.\n
    @return a new List of updated concatenations"""
    # Check that digit is between 2 and 9 (inclusive)
    try:
        int_digit = int(digit)
        if int_digit < 2 or int_digit > 9:
            return combinations
    except ValueError:
        return combinations
    # Init a list to hold an update version of combinations
    new_combo = []
    # Get the iso string for the given digit
    iso_string = TRANSLATION.get(digit)
    # If uninitialise, init combo values with all first digit's chars
    if len(combinations) == 0:
        new_combo = [char for char in iso_string]
        return new_combo
    # Generate tree-like branches from the current list
    for text in combinations:
        for char in iso_string:
            new_combo.append(text + char)
    return new_combo


def filter_valid_words(generated_texts: list, valid_words: list) -> list:
    """Returns a List object containing the intersect of the List params"""
    generated_texts_set = set(generated_texts)
    valid_words_set = set(valid_words)
    # Convert the Set intersect to a List before return
    return list(generated_texts_set.intersection(valid_words_set))


def display_possible_words(order_num: str, words) -> None:
    """
    Prints to UI, the num and accompanying contents of a data structure.\n
    Raises TypeError if an invalid type is given for words param\n
    @words is a List, Set or  Tuple data structure
    """
    if type(words) != list and type(words) != set and type(words) != tuple:
        raise TypeError("@param words must be a List, Set or Tuple")
    index = 0
    # Sort words into an ascending order
    asc_words = sorted(words)
    for word in asc_words:
        if index == 0:
            print(order_num, ":", word)
            index += 1
        else:
            print("       ", word)


def main():
    # Get file paths with correct sys.argv index, pass as arg to the read funcs
    order_nums_set = read_orders(get_file_path(1))
    words_list = read_words(get_file_path(2))
    # If an error is flagged when opening the files, print msg and abort
    if order_nums_set is None or words_list is None:
        print("Error: There was a problem with at least one of the files.")
        return
    # Get a list of words matching each order number and print to UI
    for order in order_nums_set:
        generated_texts_list = find_all_possible_combinations(order)
        real_words_set = filter_valid_words(generated_texts_list, words_list)
        display_possible_words(order, real_words_set)


if __name__ == "__main__":
    main()
